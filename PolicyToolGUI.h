#ifndef _PolicyToolGUI_PolicyToolGUI_h
#define _PolicyToolGUI_PolicyToolGUI_h

#include <CtrlLib/CtrlLib.h>

using namespace Upp;

#define LAYOUTFILE <PolicyToolGUI/PolicyToolGUI.lay>
#include <CtrlCore/lay.h>

class MainWindow : public WithMainWindowLayout<TopWindow> {
public:
	MainWindow();
};

#endif
